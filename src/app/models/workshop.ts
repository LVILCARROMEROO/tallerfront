/*
class Workshop {
  title: string;
  brief: string;
  poster: string;

  constructor(title) {
    this.title = title;
  }
}
*/

export class Workshop {
  slug: string;
  title: string;
  brief: string;
  description: string;
  poster: string;
  date: Date;
  start: Date;
  end: Date;
  vacancies: string;

  /*
  constructor(
    public slug: string,
    public title: string,
    public brief: string,
    public poster: string
  ) { }
  */
}
