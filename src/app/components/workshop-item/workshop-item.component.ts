import {Component, Input, OnInit} from '@angular/core';
import {Workshop} from '../../models/workshop';

@Component({
  selector: 'app-workshop-item',
  templateUrl: './workshop-item.component.html',
  styleUrls: ['./workshop-item.component.css']
})
export class WorkshopItemComponent implements OnInit {

  @Input() workshop: Workshop;

  constructor() { }

  ngOnInit() {
  }

}
