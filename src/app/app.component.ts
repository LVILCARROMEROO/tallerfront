import { Component } from '@angular/core';
import {Workshop} from './models/workshop';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tallerfront';
}
