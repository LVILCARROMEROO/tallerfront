import { Component, OnInit } from '@angular/core';
import {Workshop} from '../../models/workshop';
import {Router} from '@angular/router';
import {WorkshopService} from '../../services/workshop.service';

@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
  styleUrls: ['./workshops.component.css']
})
export class WorkshopsComponent implements OnInit {
  workshops: Workshop[] = [];
  constructor(
    private router: Router,
    private workshopService: WorkshopService
  ) { }

  ngOnInit() {
    this.workshopService.getWorkshops()
      .subscribe(
        (res: Workshop[]) => this.workshops = res,
        err => console.log(err)
      );
  }

  goWorkshopDetail(slug: string) {
    this.router.navigate(['/workshops', slug]);
  }

}
