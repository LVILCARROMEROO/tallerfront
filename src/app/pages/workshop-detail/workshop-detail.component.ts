import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {WorkshopService} from '../../services/workshop.service';
import {Workshop} from '../../models/workshop';

@Component({
  selector: 'app-workshop-detail',
  templateUrl: './workshop-detail.component.html',
  styleUrls: ['./workshop-detail.component.css']
})
export class WorkshopDetailComponent implements OnInit {
  workshop: Workshop;

  constructor(
    // private router: Router,
    private activatedRoute: ActivatedRoute,
    private workshopService: WorkshopService,
  ) { }

  ngOnInit() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.workshopService.getWorkshop(slug)
      .subscribe(
        (res: Workshop) => this.workshop = res,
        err => console.log(err),
      );
  }

}
