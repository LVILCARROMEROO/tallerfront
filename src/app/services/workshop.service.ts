import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkshopService {

  constructor(private http: HttpClient) { }

  getWorkshops() {
    return this.http.get('http://localhost:3000/workshop');
  }

  getWorkshop(slug: string) {
    // return this.http.get('http://n.markham.edu.pe:3000/workshop' + slug);
    return this.http.get(`http://localhost:3000/workshop/${slug}`);
  }


}
