import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WorkshopItemComponent } from './components/workshop-item/workshop-item.component';
import { WorkshopsComponent } from './pages/workshops/workshops.component';
import { WorkshopDetailComponent } from './pages/workshop-detail/workshop-detail.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    WorkshopItemComponent,
    WorkshopsComponent,
    WorkshopDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
