import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WorkshopsComponent} from './pages/workshops/workshops.component';
import {WorkshopDetailComponent} from './pages/workshop-detail/workshop-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'workshops',
    pathMatch: 'full'
  },
  {
    path: 'workshops',
    component: WorkshopsComponent
  },
  {
    path: 'workshops/:slug',
    component: WorkshopDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
